export const FETCH_NEW_CARD_DECK: string = "FETCH_NEW_CARD_DECK";
export const FETCH_NEW_CARD_DECK_FAIL: string = "FETCH_NEW_CARD_DECK_FAIL";
export const FETCH_NEW_CARD_DECK_SUCCESS: string =
  "FETCH_NEW_CARD_DECK_SUCCESS";
export const FETCH_NEW_CARD: string = "FETCH_NEW_CARD";
export const FETCH_NEW_CARD_FAIL: string = "FETCH_NEW_CARD_FAIL";
export const FETCH_NEW_CARD_SUCCESS: string = "FETCH_NEW_CARD_SUCCESS";

export const fetchNewCardDeck = () => {
  return {
    type: FETCH_NEW_CARD_DECK,
    payload: {
      request: {
        url: "/new",
      },
    },
  };
};

export const fetchNewCard = (deckId: string) => {
  return {
    type: FETCH_NEW_CARD,
    payload: {
      request: {
        url: `/${deckId}/draw/?count=1`,
      },
    },
  };
};
