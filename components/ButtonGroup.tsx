import React from "react";
import PropTypes from "prop-types";
import { Button, StyleSheet, View } from "react-native";

const ButtonGroup = (props: any) => {
  const { onClick } = props;

  return (
    <View style={styles.container}>
      <View style={styles.buttonStyle}>
        <Button onPress={() => onClick("Higher")} title="Higher"></Button>
      </View>
      <View style={styles.buttonStyle}>
        <Button onPress={() => onClick("Same")} title="Same"></Button>
      </View>
      <View style={styles.buttonStyle}>
        <Button onPress={() => onClick("Lower")} title="Lower"></Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  buttonStyle: {
    marginHorizontal: 20,
    marginTop: 5,
  },
});

ButtonGroup.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default ButtonGroup;
