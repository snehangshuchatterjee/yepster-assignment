import { combineReducers } from "redux";

import cardChooserReducer from "../reducers/cardChooser.reducer";

const RootReducer = combineReducers({
    cardChooserReducer
});

export default RootReducer;
