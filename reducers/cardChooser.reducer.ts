import {
  FETCH_NEW_CARD_DECK,
  FETCH_NEW_CARD_DECK_FAIL,
  FETCH_NEW_CARD_DECK_SUCCESS,
  FETCH_NEW_CARD,
  FETCH_NEW_CARD_FAIL,
  FETCH_NEW_CARD_SUCCESS,
} from "../actions/cardChooser.actions";

interface stateObject {
  newCard: cardObject;
  oldCard: cardObject;
  deckId: string;
  loading: boolean;
  remaining: number;
  error: boolean;
}

interface actionObject {
  type: string;
  payload: payloadObject;
}

interface payloadObject {
  data: dataObject;
}

interface dataObject {
  success: boolean;
  deck_id: string;
  shuffled: boolean;
  remaining: number;
  cards: cardObject;
}

interface cardObject {
  image: string;
  value: string;
  suit: string;
  code: string;
}

const initialState = {
  oldCard: [],
  newCard: [],
  deckId: "",
  error: false,
  loading: false,
  remaining: 0,
};

export default function cardChooserReducer(
  state: stateObject = initialState,
  action: actionObject
) {
  switch (action.type) {
    case FETCH_NEW_CARD_DECK: {
      return {
        ...state,
        loading: true,
        error: false,
      };
    }
    case FETCH_NEW_CARD_DECK_FAIL: {
      return {
        ...state,
        loading: false,
        error: true,
      };
    }
    case FETCH_NEW_CARD_DECK_SUCCESS: {
      return {
        ...state,
        loading: false,
        deckId: action.payload.data.deck_id,
      };
    }
    case FETCH_NEW_CARD: {
      return {
        ...state,
        loading: true,
        error: false,
      };
    }
    case FETCH_NEW_CARD_FAIL: {
      return {
        ...state,
        loading: false,
        error: true,
      };
    }
    case FETCH_NEW_CARD_SUCCESS: {
      return {
        ...state,
        loading: false,
        oldCard: state.newCard,
        newCard: action.payload.data.cards,
        remaining: action.payload.data.remaining,
      };
    }
    default: {
      return initialState;
    }
  }
}
