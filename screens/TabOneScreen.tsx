import React, { useEffect, useState } from "react";
import { Text, View, Image, StyleSheet } from "react-native";
import { connect } from "react-redux";

import { RootTabScreenProps } from "../types";

import { fetchNewCardDeck, fetchNewCard } from "../actions/cardChooser.actions";
import ButtonGroup from "../components/ButtonGroup";

import { CARD_ORDER } from "../constants/cardOrder";

const mapStateToProps = (state: any) => {
  return {
    deckId: state.cardChooserReducer.deckId,
    newCard: state.cardChooserReducer.newCard,
    oldCard: state.cardChooserReducer.oldCard,
    remaining: state.cardChooserReducer.remaining,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchNewCardDeck: () => dispatch(fetchNewCardDeck()),
    fetchNewCard: (deckID: string) => dispatch(fetchNewCard(deckID)),
  };
};

function TabOneScreen(props: any) {
  const {
    newCard,
    oldCard,
    deckId,
    remaining,
    fetchNewCard,
    fetchNewCardDeck,
  } = props;
  const [correctCounter, setCorrectCounter] = useState(0);
  const [userPrediction, setUserPrediction] = useState("");

  useEffect(() => {
    fetchNewCardDeck();
  }, []);

  useEffect(() => {
    if (deckId !== "") {
      fetchNewCard(deckId);
    }
  }, [deckId]);

  useEffect(() => {
    if (oldCard.length > 0) {
      compareCards();
    }
  }, [oldCard]);

  const handleClick = (prediction: string) => {
    setUserPrediction(prediction);
    fetchNewCard(deckId);
  };

  const compareCards = () => {
    const oldCardCode = oldCard[0].code;
    const newCardCode = newCard[0].code;
    let result;

    if (
      CARD_ORDER.indexOf(oldCardCode.charAt(0)) >
      CARD_ORDER.indexOf(newCardCode.charAt(0))
    ) {
      result = "Lower";
    } else if (
      CARD_ORDER.indexOf(oldCardCode.charAt(0)) <
      CARD_ORDER.indexOf(newCardCode.charAt(0))
    ) {
      result = "Higher";
    } else {
      result = "Same";
    }

    if (result === userPrediction) {
      const newCount: number = correctCounter + 1;
      setCorrectCounter(newCount);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Remaining: {remaining}</Text>
      {newCard.length > 0 && (
        <Image style={styles.cardImage} source={{ uri: newCard[0].image }} />
      )}
      {newCard.length > 0 && <ButtonGroup onClick={handleClick} />}
      {correctCounter > 1 && (
        <Text style={styles.title}>
          You have been Correct {correctCounter} times
        </Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  cardImage: {
    width: "60%",
    height: "50%",
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TabOneScreen);
